function chooseMultiplesOfThree(collection) {
  // TODO 1: 在这里写实现代码
  // eslint-disable-next-line array-callback-return,consistent-return
  return collection.filter(v => v % 3 === 0);
}

function chooseNoRepeatNumber(collection) {
  // TODO 2: 在这里写实现代码
  const ret = new Set();
  // eslint-disable-next-line array-callback-return,consistent-return
  return collection.filter(v => {
    if (!ret.has(v)) {
      ret.add(v);
      return v;
    }
  });
}

export { chooseMultiplesOfThree, chooseNoRepeatNumber };
