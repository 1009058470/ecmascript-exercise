// TODO 20: 在这里写实现代码
// TODO 19: 在这里写实现代码
import Person from './person';

export default class Teacher extends Person {
  constructor(name, age, klass) {
    super(name, age);
    // eslint-disable-next-line eqeqeq
    if (klass != undefined) this.klass = klass;
  }

  introduce() {
    // eslint-disable-next-line eqeqeq
    if (this.klass != undefined) return `${super.introduce()} I am a Teacher. I teach Class 2.`;
    return `${super.introduce()} I am a Teacher. I teach No Class.`;
  }
}
